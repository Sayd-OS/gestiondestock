package ci.saydos.gestiondestock;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.repository.query.Param;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
@EnableJpaAuditing
public class GestiondestockApplication {


	public static void main(String[] args) {
		SpringApplication.run(GestiondestockApplication.class, args);
	}

//	@Bean
//	CommandLineRunner start(){
//		return args -> {
//            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
//			String password = encoder.encode("som3R@nd0mP@$$w0rd");
//			System.out.println(password);
//	};
//
//	}}



}
