package ci.saydos.gestiondestock.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "mvtStk")
public class MvtStk extends AbstractEntity{
    @ManyToOne
    @JoinColumn(name = "id_article")
    private Article article;

    @Column(name = "date_mvt")
    private Instant dateMvt;

    @Enumerated(EnumType.STRING)
    private TypeMvtStk typeMvtStk;

    @Enumerated(EnumType.STRING)
    private SourceMvtStk sourceMvtStk;

    @Column(name = "quantite")
    private BigDecimal quantite;

    @Column(name = "prix_unitaire")
    private BigDecimal prixUnitaire;

    @Column(name = "id_entreprise")
    private Integer idEntreprise;
}
