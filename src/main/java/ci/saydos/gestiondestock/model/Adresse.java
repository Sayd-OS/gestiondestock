package ci.saydos.gestiondestock.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Embeddable
public class Adresse {
    @Column(name = "adresse_1")
    private String adresse1;
    @Column(name = "adresse_2")
    private String adresse2;
    @Column(name = "ville")
    private String ville;
    @Column(name = "code_postale")
    private String codePostale;
    @Column(name = "pays")
    private String pays;
}
