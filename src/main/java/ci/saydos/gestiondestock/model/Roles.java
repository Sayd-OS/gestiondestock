package ci.saydos.gestiondestock.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "roles")
public class Roles extends AbstractEntity{
    @Column(name = "role_name")
    private String roleName;

}
