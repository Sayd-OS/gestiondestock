package ci.saydos.gestiondestock.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "entreprise")
public class Entreprise extends AbstractEntity{
    @Column(name = "nom")
    private String nom;
    @Column(name = "description")
    private String description;
    @Embedded
    private Adresse adresse;
    @Column(name = "code_fiscal")
    private String codeFiscal;
    @Column(name = "photo")
    private String photo;
    @Column(name ="email")
    private String email;
    @Column(name ="num_tel")
    private String numTel;
    @Column(name ="site_web")
    private String siteWeb;
    @OneToMany(mappedBy = "entreprise")
    @JsonManagedReference
    private List<Utilisateur> utilisateurs;



}
