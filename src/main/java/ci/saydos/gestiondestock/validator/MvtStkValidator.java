package ci.saydos.gestiondestock.validator;

import ci.saydos.gestiondestock.dto.MvtStkDto;
import ci.saydos.gestiondestock.dto.UtilisateurDto;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MvtStkValidator {
    public static List<String> validate(MvtStkDto dto) {
        List<String> errors = new ArrayList<>();

        if(dto ==null ){
            errors.add("Veuillez renseigner la date du mouvement");
            errors.add("Veuillez renseigner la quantite du mouvement");
            errors.add("Veuillez renseigner l'article");
            errors.add("Veuillez renseigner le type du mouvement");
            return errors;
        }

        if(dto.getDateMvt() ==null) {
            errors.add("Veuillez renseigner la date du mouvement");
        }
        if(dto.getQuantite() == null || dto.getQuantite().compareTo(BigDecimal.ZERO) == 0) {
            errors.add("Veuillez renseigner la quantite du mouvement");
        }
        if(dto.getArticle() == null || dto.getArticle().getId() == null){
            errors.add("Veuillez renseigner l'article");
        }
        if(!StringUtils.hasLength(dto.getTypeMvtStk().name())) {
            errors.add("Veuillez renseigner le mot de passe de l'utilisateur");
        }

        return errors;
    }
}
