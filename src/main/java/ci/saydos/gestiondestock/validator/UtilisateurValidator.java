package ci.saydos.gestiondestock.validator;

import ci.saydos.gestiondestock.dto.ClientDto;
import ci.saydos.gestiondestock.dto.UtilisateurDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class UtilisateurValidator {
    public static List<String> validate(UtilisateurDto dto) {
        List<String> errors = new ArrayList<>();

        if(dto ==null ){
            errors.add("Veuillez renseigner le nom de l'utilisateur");
            errors.add("Veuillez renseigner la prenom de l'utilisateur");
            errors.add("Veuillez renseigner le mot de passe de l'utilisateur");
            errors.add("Veuillez renseigner l'adresse de l'utilisateur");
            errors.addAll(AdresseValidator.validate(null));
            return errors;
        }

        if(!StringUtils.hasLength(dto.getNom())) {
            errors.add("Veuillez renseigner le nom de l'utilisateur");
        }
        if(!StringUtils.hasLength(dto.getPrenom())) {
            errors.add("Veuillez renseigner la prenom de l'utilisateur");
        }
        if(!StringUtils.hasLength(dto.getMotDePasse())) {
            errors.add("Veuillez renseigner le mot de passe de l'utilisateur");
        }

        errors.addAll(AdresseValidator.validate(dto.getAdresse()));
        return errors;
    }
}
