package ci.saydos.gestiondestock.validator;

import ci.saydos.gestiondestock.dto.ClientDto;
import ci.saydos.gestiondestock.dto.FournisseurDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class FournisseurValidator {
    public static List<String> validate(FournisseurDto dto) {
        List<String> errors = new ArrayList<>();

        if(dto ==null ){
            errors.add("Veuillez renseigner le nom du fournisseur");
            errors.add("Veuillez renseigner la prenom du fournisseur");
            errors.add("Veuillez renseigner le mail du fournisseur");
            errors.add("Veuillez renseigner le numéro de téléphone du fournisseur");
            errors.addAll(AdresseValidator.validate(null));
            return errors;
        }

        if(!StringUtils.hasLength(dto.getMail())) {
            errors.add("Veuillez renseigner le nom du fournisseur");
        }
        if(!StringUtils.hasLength(dto.getPrenom())) {
            errors.add("Veuillez renseigner la prenom du fournisseur");
        }
        if(!StringUtils.hasLength(dto.getMail())) {
            errors.add("Veuillez renseigner le mail du fournisseur");
        }
        if(!StringUtils.hasLength(dto.getMail())) {
            errors.add("Veuillez renseigner le numéro de téléphone du fournisseur");
        }
        errors.addAll(AdresseValidator.validate(dto.getAdresse()));
        return errors;
    }

}
