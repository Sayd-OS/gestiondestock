package ci.saydos.gestiondestock.services.strategy;

import ci.saydos.gestiondestock.dto.ClientDto;
import ci.saydos.gestiondestock.dto.UtilisateurDto;
import ci.saydos.gestiondestock.exception.ErrorCodes;
import ci.saydos.gestiondestock.exception.InvalidOperationException;
import ci.saydos.gestiondestock.services.implementation.FlickrService;
import ci.saydos.gestiondestock.services.implementation.UtilisateurService;
import com.flickr4java.flickr.FlickrException;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.InputStream;

@Service
@Slf4j
@NoArgsConstructor
@AllArgsConstructor
public class SaveUtilisateurPhoto implements Strategy<UtilisateurDto> {

    private UtilisateurService utilisateurService;
    private FlickrService flickrService;
    @Override
    public UtilisateurDto savePhoto(Integer id, InputStream photo, String title) throws FlickrException {
        UtilisateurDto utilisateur = utilisateurService.findById(id);
        String urlPhoto = flickrService.savePhoto(photo,title);
        if(!StringUtils.hasLength(urlPhoto)){
            throw new InvalidOperationException("Erreur lors de l'enregistrement de la photo de l'utilisateur", ErrorCodes.UPDATE_PHOTO_EXCEPTION);
        }
        utilisateur.setPhoto(urlPhoto);
        return utilisateurService.save(utilisateur);
    }
}
