package ci.saydos.gestiondestock.services.strategy;

import ci.saydos.gestiondestock.dto.ArticleDto;
import ci.saydos.gestiondestock.dto.ClientDto;
import ci.saydos.gestiondestock.exception.ErrorCodes;
import ci.saydos.gestiondestock.exception.InvalidOperationException;
import ci.saydos.gestiondestock.model.Client;
import ci.saydos.gestiondestock.services.implementation.ClientService;
import ci.saydos.gestiondestock.services.implementation.FlickrService;
import com.flickr4java.flickr.FlickrException;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.InputStream;
@Service
@Slf4j
@NoArgsConstructor
@AllArgsConstructor
public class SaveClientPhoto implements Strategy<ClientDto> {
    private ClientService clientService;
    private FlickrService flickrService;
    @Override
    public ClientDto savePhoto(Integer id , InputStream photo, String title) throws FlickrException {
        ClientDto  client = clientService.findById(id);
        String urlPhoto = flickrService.savePhoto(photo,title);
        if(!StringUtils.hasLength(urlPhoto)){
            throw new InvalidOperationException("Erreur lors de l'enregistrement de la photo du client", ErrorCodes.UPDATE_PHOTO_EXCEPTION);
        }
        client.setPhoto(urlPhoto);
        return clientService.save(client);
    }
}
