package ci.saydos.gestiondestock.services.strategy;

import ci.saydos.gestiondestock.dto.ClientDto;
import ci.saydos.gestiondestock.dto.FournisseurDto;
import ci.saydos.gestiondestock.exception.ErrorCodes;
import ci.saydos.gestiondestock.exception.InvalidOperationException;
import ci.saydos.gestiondestock.services.implementation.FlickrService;
import ci.saydos.gestiondestock.services.implementation.FournisseurService;
import ci.saydos.gestiondestock.services.implementation.UtilisateurService;
import com.flickr4java.flickr.FlickrException;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.InputStream;

@Service
@Slf4j
@NoArgsConstructor
@AllArgsConstructor
public class SaveFournisseurPhoto implements Strategy<FournisseurDto> {
    private FournisseurService fournisseurService;
    private FlickrService flickrService;

    @Override
    public FournisseurDto savePhoto(Integer id, InputStream photo, String title) throws FlickrException {
        FournisseurDto fournisseur = fournisseurService.findById(id);
        String urlPhoto = flickrService.savePhoto(photo,title);
        if(!StringUtils.hasLength(urlPhoto)){
            throw new InvalidOperationException("Erreur lors de l'enregistrement de le photo du fournisseur", ErrorCodes.UPDATE_PHOTO_EXCEPTION);
        }
        fournisseur.setPhoto(urlPhoto);
        return fournisseurService.save(fournisseur);
    }
}
