package ci.saydos.gestiondestock.services.strategy;

import ci.saydos.gestiondestock.dto.ArticleDto;
import ci.saydos.gestiondestock.exception.ErrorCodes;
import ci.saydos.gestiondestock.exception.InvalidOperationException;
import ci.saydos.gestiondestock.model.Article;
import ci.saydos.gestiondestock.repository.ArticleRepository;
import ci.saydos.gestiondestock.services.implementation.ArticleService;
import ci.saydos.gestiondestock.services.implementation.FlickrService;
import com.flickr4java.flickr.FlickrException;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.InputStream;

@Service
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class SaveArticlePhoto implements Strategy<ArticleDto>{
    private FlickrService flickrService;
    private ArticleService articleService;
    @Override
    public ArticleDto savePhoto(Integer id ,InputStream photo, String title) throws FlickrException {
        ArticleDto article = articleService.findById(id);
        String urlPhoto = flickrService.savePhoto(photo,title);
        if(!StringUtils.hasLength(urlPhoto)){
            throw new InvalidOperationException("Erreur lors de l'enregistrement de la photo de l'article", ErrorCodes.UPDATE_PHOTO_EXCEPTION);
        }
        article.setPhoto(urlPhoto);
        return articleService.save(article);
    }
}
