package ci.saydos.gestiondestock.services.strategy;

import ci.saydos.gestiondestock.dto.ClientDto;
import ci.saydos.gestiondestock.dto.EntrepriseDto;
import ci.saydos.gestiondestock.dto.UtilisateurDto;
import ci.saydos.gestiondestock.exception.ErrorCodes;
import ci.saydos.gestiondestock.exception.InvalidOperationException;
import ci.saydos.gestiondestock.services.implementation.EntrepriseService;
import ci.saydos.gestiondestock.services.implementation.FlickrService;
import ci.saydos.gestiondestock.services.implementation.UtilisateurService;
import com.flickr4java.flickr.FlickrException;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.InputStream;

@Service
@Slf4j
@NoArgsConstructor
@AllArgsConstructor
public class SaveEntreprisePhoto implements Strategy<EntrepriseDto> {
    private EntrepriseService entrepriseService;
    private FlickrService flickrService;

    @Override
    public EntrepriseDto savePhoto(Integer id, InputStream photo, String title) throws FlickrException {
        EntrepriseDto entreprise = entrepriseService.findById(id);
        String urlPhoto = flickrService.savePhoto(photo,title);
        if(!StringUtils.hasLength(urlPhoto)){
            throw new InvalidOperationException("Erreur lors de l'enregistrement de la photo de l'entreprise", ErrorCodes.UPDATE_PHOTO_EXCEPTION);
        }
        entreprise.setPhoto(urlPhoto);
        return entrepriseService.save(entreprise);
    }
}
