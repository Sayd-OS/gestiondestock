package ci.saydos.gestiondestock.services;

import ci.saydos.gestiondestock.dto.CategoryDto;
import ci.saydos.gestiondestock.dto.VentesDto;

import java.util.List;

public interface IVentesService {

    VentesDto save(VentesDto dto);
    VentesDto findById(Integer id);
    VentesDto findByCode(String code);
    List<VentesDto> findAll();
    void delete(Integer id);
}
