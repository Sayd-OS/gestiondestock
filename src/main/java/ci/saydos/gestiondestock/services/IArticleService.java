package ci.saydos.gestiondestock.services;

import ci.saydos.gestiondestock.dto.ArticleDto;
import ci.saydos.gestiondestock.dto.LigneCommandeClientDto;
import ci.saydos.gestiondestock.dto.LigneCommandeFournisseurDto;
import ci.saydos.gestiondestock.dto.LigneVenteDto;

import java.util.List;

public interface IArticleService {
    ArticleDto save(ArticleDto articleDto);
    ArticleDto findById(Integer id);
    ArticleDto findByCode(String code);
    List<ArticleDto> findAll();
    List<LigneVenteDto> findHistoriqueVentes(Integer idArticle);
    List<LigneCommandeClientDto> findHistoriqueCommandeClient(Integer idArticle);
    List<LigneCommandeFournisseurDto> findHistoriqueCommandeFournisseur(Integer idArticle);
    List<ArticleDto> findAllArticleByIdCategory(Integer idCategory);
    void delete(Integer id);

}
