package ci.saydos.gestiondestock.services.implementation;

import ci.saydos.gestiondestock.dto.EntrepriseDto;
import ci.saydos.gestiondestock.dto.RolesDto;
import ci.saydos.gestiondestock.dto.UtilisateurDto;
import ci.saydos.gestiondestock.exception.ErrorCodes;
import ci.saydos.gestiondestock.exception.InvalidEntityFoundException;
import ci.saydos.gestiondestock.repository.EntrepriseRepository;
import ci.saydos.gestiondestock.repository.RolesRepository;
import ci.saydos.gestiondestock.services.IEntrepriseService;
import ci.saydos.gestiondestock.validator.EntrepriseValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class EntrepriseService implements IEntrepriseService {
    private EntrepriseRepository entrepriseRepository;
    private UtilisateurService utilisateurService;
    private RolesService rolesService;
    private PasswordEncoder passwordEncoder;


    public EntrepriseService(EntrepriseRepository entrepriseRepository,
                             UtilisateurService utilisateurService,
                             RolesRepository rolesRepository, RolesService rolesService, PasswordEncoder passwordEncoder) {
        this.entrepriseRepository = entrepriseRepository;
        this.utilisateurService = utilisateurService;
        this.rolesService = rolesService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public EntrepriseDto save(EntrepriseDto dto) {
        List<String> errors = EntrepriseValidator.validate(dto);
        if (!errors.isEmpty()) {
            log.error("Entreprise is not valid", dto);
            throw new InvalidEntityFoundException("L'entreprise n'est pas valide", ErrorCodes.ENTREPRISE_NOT_VALID, errors);
        }
        EntrepriseDto savedEntreprise = EntrepriseDto.fromEntity(
                entrepriseRepository.save(EntrepriseDto.toEntity(dto))
        );
        UtilisateurDto utilisateur = fromEntreprise(savedEntreprise);
        System.out.println(utilisateur);
        UtilisateurDto savedUser = utilisateurService.save(utilisateur);
        return savedEntreprise;

    }

    private UtilisateurDto fromEntreprise(EntrepriseDto dto) {
        RolesDto role = rolesService.findByRoleName("ADMIN");
        return UtilisateurDto.builder()
                .roles(Collections.singletonList(role))
                .adresse(dto.getAdresse())
                .nom(dto.getNom())
                .prenom(dto.getCodeFiscal())
                .email(dto.getEmail())
                .motDePasse(passwordEncoder.encode(generateRandomPassword()))
                .entreprise(dto)
                .dateDeNaissance(Instant.now())
                .photo(dto.getPhoto())
                .build();
    }

    private String generateRandomPassword() {
        return "som3R@nd0mP@$$w0rd";
    }

    @Override
    public EntrepriseDto findById(Integer id) {
        if (id == null) {
            log.error("Entreprise ID is null");
        }
        return entrepriseRepository.findById(id)
                .map(EntrepriseDto::fromEntity)
                .orElseThrow(() -> new InvalidEntityFoundException(
                        "Aucune Entreprise avec l'ID = " + id + "n'a ete trouve dans la BDD",
                        ErrorCodes.ENTREPRISE_NOT_FOUND)
                );
    }

    @Override
    public List<EntrepriseDto> findAll() {
        return entrepriseRepository.findAll().stream()
                .map(EntrepriseDto::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public void delete(Integer id) {
        if (id == null) {
            log.error("Entreprise ID is null");
        }
        entrepriseRepository.deleteById(id);
    }
}
