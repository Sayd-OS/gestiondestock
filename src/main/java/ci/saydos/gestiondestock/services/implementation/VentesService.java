package ci.saydos.gestiondestock.services.implementation;


import ci.saydos.gestiondestock.dto.*;
import ci.saydos.gestiondestock.exception.ErrorCodes;
import ci.saydos.gestiondestock.exception.InvalidEntityFoundException;
import ci.saydos.gestiondestock.exception.InvalidOperationException;
import ci.saydos.gestiondestock.model.*;
import ci.saydos.gestiondestock.repository.ArticleRepository;
import ci.saydos.gestiondestock.repository.LigneVenteRepository;
import ci.saydos.gestiondestock.repository.VentesRepository;
import ci.saydos.gestiondestock.services.IVentesService;
import ci.saydos.gestiondestock.validator.VentesValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class VentesService implements IVentesService {
    private final LigneVenteRepository ligneVenteRepository;
    private final VentesRepository ventesRepository;
    private final ArticleRepository articleRepository;
    @Autowired
    private MvtStkService mvtStkService;

    public VentesService(ArticleRepository articleRepository,
                         VentesRepository ventesRepository,
                         LigneVenteRepository ligneVenteRepository) {
        this.articleRepository = articleRepository;
        this.ventesRepository = ventesRepository;
        this.ligneVenteRepository = ligneVenteRepository;
    }

    @Override
    public VentesDto save(VentesDto dto) {
        List<String> errors = VentesValidator.validate(dto);
        if (!errors.isEmpty()) {
            log.error("Vente  is not valid", dto);
            throw new InvalidEntityFoundException("La Vente n'est pas valide", ErrorCodes.VENTE_NOT_VALID, errors);
        }

        List<String> articleErrors = new ArrayList<String>();
        dto.getLigneVente().forEach(ligneVenteDto -> {
                    Optional<Article> article = articleRepository.findById(ligneVenteDto.getArticle().getId());
                    if (article.isEmpty()) {
                        articleErrors.add("L'article avec l'ID" + ligneVenteDto.getArticle().getId() + "n'existe pas");
                    }
                });

        if (!articleErrors.isEmpty()) {
            log.error("One or more articles were not found in DB, {}", errors);
            throw new InvalidEntityFoundException("un ou plusieurs articles n'ont pas ete trouve dans la BDD", ErrorCodes.ARTICLE_NOT_FOUND, articleErrors);
        }

        Ventes savedVentes = ventesRepository.save(VentesDto.toEntity(dto));

            dto.getLigneVente().forEach(ligneVenteDto -> {
                LigneVente ligneVente = LigneVenteDto.toEntity(ligneVenteDto);
                ligneVenteRepository.save(ligneVente);
                updateMvtStk(ligneVente);
            });

        return VentesDto.fromEntity(savedVentes);
    }

    @Override
    public VentesDto findById(Integer id) {
        if (id == null) {
            log.error("Vente ID is null");
        }
        return ventesRepository.findById(id)
                .map(VentesDto::fromEntity)
                .orElseThrow(() -> new InvalidEntityFoundException(
                        "Aucune Vente avec l'ID = " +id+"n'a ete trouve dans la BDD",
                        ErrorCodes.VENTE_NOT_FOUND)
                );
    }

    @Override
    public VentesDto findByCode(String code) {
        if (code == null) {
            log.error("Vente CODE is null");
        }
        return ventesRepository.findVentesByCode(code)
                .map(VentesDto::fromEntity)
                .orElseThrow(() -> new InvalidEntityFoundException(
                        "Aucune Vente avec le code = " +code+"n'a ete trouve dans la BDD",
                        ErrorCodes.VENTE_NOT_FOUND)
                );
    }

    @Override
    public List<VentesDto> findAll() {
        return ventesRepository.findAll().stream()
                .map(VentesDto::fromEntity)
                .collect(Collectors.toList());
    }
    @Override
    public void delete(Integer id) {
        if (id == null) {
            log.error("Vente ID is null");
        }
        List<LigneVente> ligneVentes = ligneVenteRepository.findAllByVente_Id(id);
        if(!ligneVentes.isEmpty()){
            throw new InvalidOperationException("Impossible de supprimer une vente qui a déja utilisée",
                    ErrorCodes.VENTE_ALREADY_IN_USE);
        }
        ventesRepository.deleteById(id);
    }

    private void updateMvtStk(LigneVente lig){

            MvtStkDto mvtStkDto = MvtStkDto.builder()
                    .article(ArticleDto.fromEntity(lig.getArticle()))
                    .dateMvt(Instant.now())
                    .typeMvtStk(TypeMvtStk.SORTIE)
                    .sourceMvtStk(SourceMvtStk.VENTE)
                    .idEntreprise(lig.getIdEntreprise())
                    .build();
            mvtStkService.sortieStock(mvtStkDto);

    }
}
