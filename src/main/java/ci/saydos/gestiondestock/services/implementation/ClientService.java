package ci.saydos.gestiondestock.services.implementation;

import ci.saydos.gestiondestock.dto.ClientDto;
import ci.saydos.gestiondestock.dto.ClientDto;
import ci.saydos.gestiondestock.exception.ErrorCodes;
import ci.saydos.gestiondestock.exception.InvalidEntityFoundException;
import ci.saydos.gestiondestock.exception.InvalidOperationException;
import ci.saydos.gestiondestock.model.Article;
import ci.saydos.gestiondestock.model.CommandeClient;
import ci.saydos.gestiondestock.repository.ClientRepository;
import ci.saydos.gestiondestock.repository.CommandeClientRepository;
import ci.saydos.gestiondestock.services.IClientService;
import ci.saydos.gestiondestock.validator.ClientValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ClientService implements IClientService {
    private final CommandeClientRepository commandeClientRepository;
    private final ClientRepository clientRepository;

    public ClientService(ClientRepository clientRepository,
                         CommandeClientRepository commandeClientRepository) {
        this.clientRepository = clientRepository;
        this.commandeClientRepository = commandeClientRepository;
    }


    @Override
    public ClientDto save(ClientDto dto) {
        List<String> errors = ClientValidator.validate(dto);
        if (!errors.isEmpty()) {
            log.error("Client is not valid",dto);
            throw new InvalidEntityFoundException("Le client n'est pas valide", ErrorCodes.CLIENT_NOT_VALID, errors);
        }
        return ClientDto.fromEntity(clientRepository.save(ClientDto.toEntity(dto)));
    }

    @Override
    public ClientDto findById(Integer id) {
        if (id == null) {
            log.error("Client ID is null");
        }
        return clientRepository.findById(id)
                .map(ClientDto::fromEntity)
                .orElseThrow(() -> new InvalidEntityFoundException(
                        "Aucun Client avec l'ID = " +id+"n'a ete trouve dans la BDD",
                        ErrorCodes.CLIENT_NOT_FOUND)
                );
    }
    @Override
    public List<ClientDto> findAll() {
        return clientRepository.findAll().stream()
                .map(ClientDto::fromEntity)
                .collect(Collectors.toList());
    }
    @Override
    public void delete(Integer id) {
        if (id == null) {
            log.error("Client ID is null");
        }
        List<CommandeClient> clients = commandeClientRepository.findAllByClientId(id);
        if(!clients.isEmpty()){
            throw new InvalidOperationException("Impossible de supprimer un client qui a déja des commandes",
                    ErrorCodes.CLIENT_ALREADY_IN_USE);
        }
        clientRepository.deleteById(id);
    }
}
