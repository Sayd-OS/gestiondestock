package ci.saydos.gestiondestock.services.implementation;

import ci.saydos.gestiondestock.dto.ArticleDto;
import ci.saydos.gestiondestock.dto.LigneCommandeClientDto;
import ci.saydos.gestiondestock.dto.LigneCommandeFournisseurDto;
import ci.saydos.gestiondestock.dto.LigneVenteDto;
import ci.saydos.gestiondestock.exception.ErrorCodes;
import ci.saydos.gestiondestock.exception.InvalidEntityFoundException;
import ci.saydos.gestiondestock.exception.InvalidOperationException;
import ci.saydos.gestiondestock.model.LigneCommandeClient;
import ci.saydos.gestiondestock.model.LigneCommandeFournisseur;
import ci.saydos.gestiondestock.model.LigneVente;
import ci.saydos.gestiondestock.repository.*;
import ci.saydos.gestiondestock.services.IArticleService;
import ci.saydos.gestiondestock.validator.ArticleValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ArticleService implements IArticleService {
    private final CommandeClientRepository commandeClientRepository;
    private final CommandeFournisseurRepository commandeFournisseurRepository;
    private final ArticleRepository articleRepository;
    private final VentesRepository ventesRepository;
    private final LigneVenteRepository ligneVenteRepository;
    private final LigneCommandeClientRepository ligneCommandeClientRepository;
    private final LigneCommandeFournisseurRepository ligneCommandeFournisseurRepository;

    public ArticleService(ArticleRepository articleRepository,
                          VentesRepository ventesRepository,
                          LigneVenteRepository ligneVenteRepository,
                          LigneCommandeClientRepository ligneCommandeClientRepository,
                          LigneCommandeFournisseurRepository ligneCommandeFournisseurRepository,
                          CommandeFournisseurRepository commandeFournisseurRepository,
                          CommandeClientRepository commandeClientRepository) {

        this.articleRepository = articleRepository;
        this.ventesRepository = ventesRepository;
        this.ligneVenteRepository = ligneVenteRepository;
        this.ligneCommandeClientRepository = ligneCommandeClientRepository;
        this.ligneCommandeFournisseurRepository = ligneCommandeFournisseurRepository;
        this.commandeFournisseurRepository = commandeFournisseurRepository;
        this.commandeClientRepository = commandeClientRepository;
    }

    @Override
    public ArticleDto save(ArticleDto dto) {
        List<String> errors = ArticleValidator.validate(dto);
        if (!errors.isEmpty()) {
            log.error("Article is not valid",dto);
            throw new InvalidEntityFoundException("L'article n'est pas valide",
                    ErrorCodes.ARTICLE_NOT_VALID, errors);

        }
        return ArticleDto.fromEntity(articleRepository.save(ArticleDto.toEntity(dto)));
    }

    @Override
    public ArticleDto findById(Integer id) {
        if (id == null) {
            log.error("Article ID is null");
        }
        return articleRepository.findById(id)
                .map(ArticleDto::fromEntity)
                .orElseThrow(() -> new InvalidEntityFoundException(
                        "Aucun Article avec l'ID = " +id+"n'a ete trouve dans la BDD",
                        ErrorCodes.ARTICLE_NOT_FOUND)
                );
    }

    @Override
    public ArticleDto findByCode(String code) {
        if (code == null) {
            log.error("CODE Article is null");
        }
        return articleRepository.findArticleByCodeArticle(code)
                .map(ArticleDto::fromEntity)
                .orElseThrow(() -> new InvalidEntityFoundException(
                        "Aucun Article avec le code = " +code +"n'a ete trouve dans la BDD",
                        ErrorCodes.ARTICLE_NOT_FOUND)
                );
    }

    @Override
    public List<ArticleDto> findAll() {
        return articleRepository.findAll().stream()
                .map(ArticleDto::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public List<LigneVenteDto> findHistoriqueVentes(Integer idArticle) {
        return ligneVenteRepository.findAllByArticleId(idArticle).stream()
                .map(LigneVenteDto::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public List<LigneCommandeClientDto> findHistoriqueCommandeClient(Integer idArticle) {
        return ligneCommandeClientRepository.findAllByArticleId(idArticle).stream()
                .map(LigneCommandeClientDto::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public List<LigneCommandeFournisseurDto> findHistoriqueCommandeFournisseur(Integer idArticle) {
        return ligneCommandeFournisseurRepository.findAllByArticleId(idArticle).stream()
                .map(LigneCommandeFournisseurDto::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public List<ArticleDto> findAllArticleByIdCategory(Integer idCategory) {
        return articleRepository.findAllByCategoryId(idCategory).stream()
                .map(ArticleDto::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public void delete(Integer id) {
      if (id == null) {
          log.error("Article ID is null");
          return;
      }
      List<LigneCommandeClient> ligneCommandeClients = ligneCommandeClientRepository.findAllByArticleId(id);
        if(!ligneCommandeClients.isEmpty()){
            log.warn("Impossible de supprimer un article deja utilise dans des commande clients");
            throw new InvalidOperationException("Impossible de supprimer un article deja utilise dans des commande clients",
                    ErrorCodes.ARTICLE_ALREADY_IN_USE);
        }
        List<LigneCommandeFournisseur> ligneCommandeFournisseurs = ligneCommandeFournisseurRepository.findAllByArticleId(id);
        if(!ligneCommandeFournisseurs.isEmpty()){
            log.warn("Impossible de supprimer un article deja utilise dans des commandes fournisseurs");
            throw new InvalidOperationException("Impossible de supprimer un article deja utilise dans des commande fournisseur",
                    ErrorCodes.ARTICLE_ALREADY_IN_USE);
        }
        List<LigneVente> ligneVentes = ligneVenteRepository.findAllByArticleId(id);
        if(!ligneCommandeFournisseurs.isEmpty()){
            log.warn("Impossible de supprimer un article deja utilise dans des ventes");
            throw new InvalidOperationException("Impossible de supprimer un article deja utilise dans des ventes",
                    ErrorCodes.ARTICLE_ALREADY_IN_USE);
        }
      articleRepository.deleteById(id);
    }
}
