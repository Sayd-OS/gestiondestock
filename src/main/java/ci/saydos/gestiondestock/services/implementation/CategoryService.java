package ci.saydos.gestiondestock.services.implementation;

import ci.saydos.gestiondestock.dto.CategoryDto;
import ci.saydos.gestiondestock.exception.ErrorCodes;
import ci.saydos.gestiondestock.exception.InvalidEntityFoundException;
import ci.saydos.gestiondestock.exception.InvalidOperationException;
import ci.saydos.gestiondestock.model.Article;
import ci.saydos.gestiondestock.repository.ArticleRepository;
import ci.saydos.gestiondestock.repository.CategoryRepository;
import ci.saydos.gestiondestock.services.ICategoryService;
import ci.saydos.gestiondestock.validator.CategoryValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CategoryService implements ICategoryService {
    @Autowired
    private ArticleRepository articleRepository;
    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public CategoryDto save(CategoryDto dto) {
        List<String> errors = CategoryValidator.validate(dto);
        if (!errors.isEmpty()) {
            log.error("Category is not valid",dto);
            throw new InvalidEntityFoundException("La Categorie n'est pas valide", ErrorCodes.CATEGORY_NOT_VALID, errors);

        }
        return CategoryDto.fromEntity(categoryRepository.save(CategoryDto.toEntity(dto)));
    }

    @Override
    public CategoryDto findById(Integer id) {
        if (id == null) {
            log.error("Category ID is null");
        }
        return categoryRepository.findById(id)
                .map(CategoryDto::fromEntity)
                .orElseThrow(() -> new InvalidEntityFoundException(
                        "Aucune Category avec l'ID = " +id+"n'a ete trouve dans la BDD",
                        ErrorCodes.ARTICLE_NOT_FOUND)
                );
    }

    @Override
    public CategoryDto findByCode(String code) {
        if (code == null) {
            log.error("Category CODE is null");
        }
        return categoryRepository.findCategoryByCode(code)
                .map(CategoryDto::fromEntity)
                .orElseThrow(() -> new InvalidEntityFoundException(
                        "Aucune Category avec le code = " +code+"n'a ete trouve dans la BDD",
                        ErrorCodes.ARTICLE_NOT_FOUND)
                );
    }

    @Override
    public List<CategoryDto> findAll() {
        return categoryRepository.findAll().stream()
                .map(CategoryDto::fromEntity)
                .collect(Collectors.toList());
    }
    @Override
    public void delete(Integer id) {
        if (id == null) {
            log.error("Category ID is null");
        }
        List<Article> articles = articleRepository.findAllByCategoryId(id);
        if(!articles.isEmpty()){
            throw new InvalidOperationException("Impossible de supprimer de supprimer une categorie déja utilisée",
                    ErrorCodes.CATEGORY_ALREADY_IN_USE);
        }
        categoryRepository.deleteById(id);
    }
}
