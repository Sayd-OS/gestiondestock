package ci.saydos.gestiondestock.services.implementation;

import ci.saydos.gestiondestock.dto.RolesDto;
import ci.saydos.gestiondestock.dto.UtilisateurDto;
import ci.saydos.gestiondestock.model.Roles;
import ci.saydos.gestiondestock.repository.RolesRepository;
import ci.saydos.gestiondestock.repository.UtilisateurRepository;
import ci.saydos.gestiondestock.services.IRoles;
import org.springframework.stereotype.Service;

@Service
public class RolesService implements IRoles {
    private final RolesRepository rolesRepository;
    private final UtilisateurService utilisateurService;

    public RolesService(RolesRepository rolesRepository, UtilisateurRepository userRepository, UtilisateurService utilisateurService) {
        this.rolesRepository = rolesRepository;

        this.utilisateurService = utilisateurService;
    }

    @Override
    public RolesDto addNewRole(RolesDto role) {
        return RolesDto.fromEntity(rolesRepository.save(RolesDto.toEntity(role)));
    }

    @Override
    public void addRoleToUser(String email, String roleName) {
        UtilisateurDto user = utilisateurService.findByEmail(email);
       Roles role = rolesRepository.findByRoleName(roleName);
        user.getRoles().add(RolesDto.fromEntity(role));
    }

    @Override
    public RolesDto findByRoleName(String roleName) {
        return RolesDto.fromEntity(rolesRepository.findByRoleName(roleName));
    }
}
