package ci.saydos.gestiondestock.services;

import ci.saydos.gestiondestock.dto.ChangerMotDePasseUtilisateurDto;
import ci.saydos.gestiondestock.dto.UtilisateurDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

public interface IUtilisateurService {
    UtilisateurDto save(UtilisateurDto dto);
    UtilisateurDto findById(Integer id);
    List<UtilisateurDto> findAll();
    UtilisateurDto findByEmail(String email);
    UtilisateurDto changeMotDePasse(ChangerMotDePasseUtilisateurDto dto);
    void delete(Integer id);
}
