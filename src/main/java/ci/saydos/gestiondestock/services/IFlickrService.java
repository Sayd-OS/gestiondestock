package ci.saydos.gestiondestock.services;

import com.flickr4java.flickr.FlickrException;

import java.io.InputStream;

public interface IFlickrService {
    String savePhoto(InputStream photo, String title) throws FlickrException;
}
