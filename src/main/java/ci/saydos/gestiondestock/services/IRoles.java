package ci.saydos.gestiondestock.services;

import ci.saydos.gestiondestock.dto.RolesDto;

public interface IRoles {
    RolesDto addNewRole(RolesDto role);
    void addRoleToUser(String email, String roleName);

    RolesDto findByRoleName(String roleName);

}
