package ci.saydos.gestiondestock.services;

import ci.saydos.gestiondestock.dto.CommandeClientDto;
import ci.saydos.gestiondestock.dto.CommandeFournisseurDto;
import ci.saydos.gestiondestock.dto.LigneCommandeFournisseurDto;
import ci.saydos.gestiondestock.model.EtatCommande;

import java.math.BigDecimal;
import java.util.List;

public interface ICommandeFournisseurService {
    CommandeFournisseurDto save(CommandeFournisseurDto dto);
    CommandeFournisseurDto updateEtatCommande(Integer idCommande, EtatCommande etatCommande);
    CommandeFournisseurDto updateQuantiteCommande(Integer idCommande, Integer idLigneCommande, BigDecimal quantite);
    CommandeFournisseurDto updateFournisseur(Integer idCommande, Integer idFournisseur);
    CommandeFournisseurDto updateArticle(Integer idCommande, Integer idLigneCommande, Integer idArticle);
    CommandeFournisseurDto deleteArticle(Integer idCommande, Integer idLigneCommande) ;
    CommandeFournisseurDto findById(Integer id);
    CommandeFournisseurDto findByCode(String code);
    List<CommandeFournisseurDto> findAll();
    List<LigneCommandeFournisseurDto> findAllLigneCommandesFournisseurByCommandeFournisseurId(Integer idCommande);
    void delete(Integer id);
}
