package ci.saydos.gestiondestock.services;

import ci.saydos.gestiondestock.dto.CommandeClientDto;
import ci.saydos.gestiondestock.dto.LigneCommandeClientDto;
import ci.saydos.gestiondestock.model.EtatCommande;

import java.math.BigDecimal;
import java.util.List;

public interface ICommandeClientService {
    CommandeClientDto save(CommandeClientDto dto);
    CommandeClientDto updateEtatCommande(Integer idCommande, EtatCommande etatCommande);
    CommandeClientDto updateQuantiteCommande(Integer idCommande, Integer idLigneCommande, BigDecimal quantite);
    CommandeClientDto updateClient(Integer idCommande, Integer idClient);
    CommandeClientDto updateArticle(Integer idCommande, Integer idLigneCommande, Integer idArticle);
    CommandeClientDto deleteArticle(Integer idCommande, Integer idLigneCommande) ;
    CommandeClientDto findById(Integer id);
    CommandeClientDto findByCode(String code);
    List<CommandeClientDto> findAll();
    List<LigneCommandeClientDto> findAllLigneCommandesClientByCommandeClientId(Integer idCommande);
    void delete(Integer id);
}
