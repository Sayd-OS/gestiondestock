package ci.saydos.gestiondestock.services;

import ci.saydos.gestiondestock.dto.CategoryDto;
import ci.saydos.gestiondestock.dto.EntrepriseDto;

import java.util.List;

public interface IEntrepriseService {
    EntrepriseDto save(EntrepriseDto dto);
    EntrepriseDto findById(Integer id);
    List<EntrepriseDto> findAll();
    void delete(Integer id);
}
