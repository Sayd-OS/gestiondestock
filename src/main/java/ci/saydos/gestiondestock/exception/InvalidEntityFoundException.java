package ci.saydos.gestiondestock.exception;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class InvalidEntityFoundException extends RuntimeException{
    private ErrorCodes errorCode;
    private List<String> errors;

    public InvalidEntityFoundException(String message){
        super(message);
    }

    public InvalidEntityFoundException(String message, ErrorCodes errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public InvalidEntityFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidEntityFoundException(String message, Throwable cause, ErrorCodes errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }


    public InvalidEntityFoundException(String message, ErrorCodes errorCode, List<String> errors) {
        super(message);
        this.errorCode = errorCode;
        this.errors = errors;
    }

}
