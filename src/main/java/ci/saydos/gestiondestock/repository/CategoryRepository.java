package ci.saydos.gestiondestock.repository;

import ci.saydos.gestiondestock.dto.CategoryDto;
import ci.saydos.gestiondestock.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
    Optional<Category> findCategoryByCode(String code);
}