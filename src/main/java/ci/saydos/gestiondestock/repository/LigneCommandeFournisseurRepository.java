package ci.saydos.gestiondestock.repository;

import ci.saydos.gestiondestock.model.LigneCommandeClient;
import ci.saydos.gestiondestock.model.LigneCommandeFournisseur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LigneCommandeFournisseurRepository extends JpaRepository<LigneCommandeFournisseur, Integer> {
    List<LigneCommandeFournisseur> findAllByCommandeFournisseur_Id(Integer id);
    List<LigneCommandeFournisseur> findAllByArticleId(Integer id);
}