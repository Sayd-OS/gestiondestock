package ci.saydos.gestiondestock.repository;

import ci.saydos.gestiondestock.model.Category;
import ci.saydos.gestiondestock.model.LigneVente;
import ci.saydos.gestiondestock.model.Ventes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface VentesRepository extends JpaRepository<Ventes, Integer> {
    Optional<Ventes> findVentesByCode(String code);

}