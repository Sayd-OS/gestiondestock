package ci.saydos.gestiondestock.repository;

import ci.saydos.gestiondestock.model.LigneCommandeClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LigneCommandeClientRepository extends JpaRepository<LigneCommandeClient, Integer> {
    List<LigneCommandeClient> findAllByCommandeClient_Id(Integer id);
    List<LigneCommandeClient> findAllByArticleId(Integer id);
}