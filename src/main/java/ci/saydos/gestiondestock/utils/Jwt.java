package ci.saydos.gestiondestock.utils;

import ci.saydos.gestiondestock.model.authentification.ExtendedUser;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.stream.Collectors;

@Component
public class Jwt {
    public String generateAccessToken(ExtendedUser user) {

        Algorithm algo1 = Algorithm.HMAC256(Constants.SECRET);
        return JWT.create()
                .withSubject(user.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + Constants.EXPIRE_ACCESS_TOKEN))
                .withClaim("roles", user.getAuthorities().stream().map(ga -> ga.getAuthority()).collect(Collectors.toList()))
                .withClaim("idEntreprise", user.getIdEntreprise().toString())
                .sign(algo1);
    }

    public String generateRefreshToken(ExtendedUser user) {
        Algorithm algo1 = Algorithm.HMAC256(Constants.SECRET);
        return JWT.create()
                .withSubject(user.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + Constants.EXPIRE_REFRESH_TOKEN))
                .sign(algo1);
    }
}
