package ci.saydos.gestiondestock.utils;

public interface Constants {
    String APP_ROOT ="gestiondestock/v1";
    String AUTHENTIFICATION_ENDPOINT =APP_ROOT+"/auth";

    String PREFIX = "Bearer ";
    String SECRET = "mySecret1234";
    String AUTH_HEADER = "Authorization";
    long EXPIRE_ACCESS_TOKEN= 2*60*1000;
    long EXPIRE_REFRESH_TOKEN= 15*60*1000;
}
