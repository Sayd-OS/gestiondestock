package ci.saydos.gestiondestock.controller;

import ci.saydos.gestiondestock.controller.api.EntrepriseApi;
import ci.saydos.gestiondestock.dto.EntrepriseDto;
import ci.saydos.gestiondestock.services.implementation.EntrepriseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EntrepriseController implements EntrepriseApi {

    @Autowired
    private EntrepriseService entrepriseService;

    @Override
    public ResponseEntity<EntrepriseDto> save(EntrepriseDto dto) {
        return ResponseEntity.ok(entrepriseService.save(dto));
    }

    @Override
    public ResponseEntity<EntrepriseDto> findById(Integer id) {
        return  ResponseEntity.ok(entrepriseService.findById(id)) ;
    }

    @Override
    public ResponseEntity<List<EntrepriseDto>> findAll() {
        return  ResponseEntity.ok(entrepriseService.findAll()) ;
    }

    @Override
    public ResponseEntity<Void> delete(Integer id) {
        entrepriseService.delete(id);
        return  ResponseEntity.ok().build();
    }
}
