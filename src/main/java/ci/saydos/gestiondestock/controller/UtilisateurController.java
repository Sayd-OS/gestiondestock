package ci.saydos.gestiondestock.controller;

import ci.saydos.gestiondestock.controller.api.UtilisateurApi;
import ci.saydos.gestiondestock.dto.ChangerMotDePasseUtilisateurDto;
import ci.saydos.gestiondestock.dto.UtilisateurDto;
import ci.saydos.gestiondestock.services.implementation.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
public class UtilisateurController implements UtilisateurApi {
    @Autowired
    private UtilisateurService utilisateurService;


    @Override
    public UtilisateurDto save(UtilisateurDto dto) {
        return utilisateurService.save(dto);
    }

    @Override
    public UtilisateurDto findById(Integer id) {
        return utilisateurService.findById(id);
    }

    @Override
    public List<UtilisateurDto> findAll() {
        return utilisateurService.findAll();
    }

    @Override
    public UtilisateurDto findByEmail(String email) {
        return utilisateurService.findByEmail(email);
    }

    @Override
    public UtilisateurDto changeMotDePasse(ChangerMotDePasseUtilisateurDto dto) {
        return utilisateurService.changeMotDePasse(dto);
    }

    @Override
    public void delete(Integer id) {
    utilisateurService.delete(id);
    }
}
