package ci.saydos.gestiondestock.controller;

import ci.saydos.gestiondestock.controller.api.PhotoApi;
import ci.saydos.gestiondestock.services.strategy.StrategyPhotContext;
import com.flickr4java.flickr.FlickrException;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@AllArgsConstructor
@NoArgsConstructor
public class PhotoController implements PhotoApi {

    private StrategyPhotContext strategyPhotContext;

    @Override
    public Object savePhoto(String context, Integer id, MultipartFile photo, String title) throws IOException, FlickrException {
        return strategyPhotContext.savePhoto(context, id, photo.getInputStream(), title);
    }
}
