package ci.saydos.gestiondestock.controller;

import ci.saydos.gestiondestock.controller.api.VentesApi;
import ci.saydos.gestiondestock.dto.CategoryDto;
import ci.saydos.gestiondestock.dto.VentesDto;
import ci.saydos.gestiondestock.services.implementation.VentesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
public class VentesController implements VentesApi {
    @Autowired
    private VentesService ventesService;


    @Override
    public VentesDto save(VentesDto dto) {
        return ventesService.save(dto);
    }

    @Override
    public VentesDto findById(Integer id) {
        return findById(id);
    }

    @Override
    public VentesDto findByCode(String code) {
        return ventesService.findByCode(code);
    }

    @Override
    public List<VentesDto> findAll() {
        return ventesService.findAll();
    }

    @Override
    public void delete(Integer id) {
        ventesService.delete(id);
    }
}
