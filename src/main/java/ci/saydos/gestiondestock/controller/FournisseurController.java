package ci.saydos.gestiondestock.controller;

import ci.saydos.gestiondestock.controller.api.FournisseurApi;
import ci.saydos.gestiondestock.dto.FournisseurDto;
import ci.saydos.gestiondestock.services.implementation.FournisseurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
public class FournisseurController implements FournisseurApi {

    @Autowired
    private FournisseurService fournisseurService;

    @Override
    public FournisseurDto save(FournisseurDto dto) {
        return fournisseurService.save(dto);
    }

    @Override
    public FournisseurDto findById(Integer id) {
        return fournisseurService.findById(id);
    }

    @Override
    public List<FournisseurDto> findAll() {
        return fournisseurService.findAll();
    }

    @Override
    public void delete(Integer id) {
    fournisseurService.delete(id);
    }
}
