package ci.saydos.gestiondestock.controller.api;

import ci.saydos.gestiondestock.dto.CategoryDto;
import ci.saydos.gestiondestock.dto.VentesDto;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static ci.saydos.gestiondestock.utils.Constants.APP_ROOT;

@Api(value = APP_ROOT +"/ventes",tags = {"ventes"})
@CrossOrigin(origins = "http://localhost:4200")
public interface VentesApi {

    @PostMapping(APP_ROOT +"/ventes/create")
    VentesDto save(@RequestBody VentesDto dto);
    @GetMapping(APP_ROOT + "/ventes/{idVente}")
    VentesDto findById(@PathVariable("idVente") Integer id);
    @GetMapping(APP_ROOT + "/ventes/{codVente}")
    VentesDto findByCode(@PathVariable("codVente") String code);
    @GetMapping(APP_ROOT + "/ventes/all")
    List<VentesDto> findAll();
    @DeleteMapping(APP_ROOT + "/ventes/{idVente}")
    void delete(@PathVariable("idVente") Integer id);
}
