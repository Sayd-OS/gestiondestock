package ci.saydos.gestiondestock.controller.api;

import ci.saydos.gestiondestock.dto.CategoryDto;
import ci.saydos.gestiondestock.dto.MvtStkDto;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

import static ci.saydos.gestiondestock.utils.Constants.APP_ROOT;

@Api(value = APP_ROOT +"/mvtStk",tags = {"mvtStk"})
@CrossOrigin(origins = "http://localhost:4200")
public interface MvtStkApi {
    @GetMapping(APP_ROOT+"/mvtStk/stockReel/{idArticle}")
    BigDecimal stockReelArticle(@PathVariable("idArticle") Integer idArticle);
    @GetMapping(APP_ROOT+"/mvtStk/filter/article/{idArticle}")
    List<MvtStkDto> mvtStkArticle(@PathVariable("idArticle")Integer idArticle);
    @PostMapping(APP_ROOT+"/mvtStk/entree")
    MvtStkDto entreeStock(@RequestBody MvtStkDto dto);
    @PostMapping(APP_ROOT+"/mvtStk/sortie")
    MvtStkDto sortieStock(@RequestBody MvtStkDto dto);
    @PostMapping(APP_ROOT+"/mvtStk/correctionPos")
    MvtStkDto correctionStockPos(@RequestBody MvtStkDto dto);
    @PostMapping(APP_ROOT+"/mvtStk/correctionNeg")
    MvtStkDto correctionStockNeg(@RequestBody MvtStkDto dto);
}
