package ci.saydos.gestiondestock.controller.api;

import ci.saydos.gestiondestock.dto.CategoryDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static ci.saydos.gestiondestock.utils.Constants.APP_ROOT;
@Api(value = APP_ROOT +"/categories",tags = {"categories"})
@CrossOrigin(origins = "http://localhost:4200")
public interface CategoryApi {

    @PostMapping(value = APP_ROOT +"/categories/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Enregistrer une categorie", notes ="Cette méthode permet d'enregistrer ou modifier une categorie", response = CategoryDto.class)
    @ApiResponses(value ={
            @ApiResponse(code =200, message = "La categorie a ete cree / modifié"),
            @ApiResponse(code =400, message = "La categorie n'est pas valide")
    })
    CategoryDto save(@RequestBody CategoryDto dto);

    @GetMapping(value = APP_ROOT +"/categories/{idCategory}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Rechercher une categorie par son ID", notes ="Cette méthode permet de rechercher une categorie par son ID", response = CategoryDto.class)
    @ApiResponses(value ={
            @ApiResponse(code =200, message = "La categorie a ete cree / modifié"),
            @ApiResponse(code =400, message = "La categorie n'est pas valide")
    })
    CategoryDto findById(@PathVariable("idCategory") Integer id);

    @GetMapping(value = APP_ROOT +"/categories/{codeCategory}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Rechercher une categorie par son Code", notes ="Cette méthode permet de rechercher une categorie par son Code", response = CategoryDto.class)
    @ApiResponses(value ={
            @ApiResponse(code =200, message = "La categorie a ete trouve dans la BDD/ modifié"),
            @ApiResponse(code =400, message = "Aucune categorie n'existe dans la BDD avec l'ID fourni")
    })
    CategoryDto findByCode(@PathVariable("codeCategory") String code);
    @GetMapping(value = APP_ROOT +"/categories/all",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Enregistrer une categorie", notes ="Cette méthode permet d'enregistrer ou modifier une categorie",
            responseContainer = "List<CategoryDto>")
    @ApiResponses(value ={
            @ApiResponse(code =200, message = "La categorie a ete cree / modifié"),
            @ApiResponse(code =400, message = "La categorie n'est pas valide")
    })
    List<CategoryDto> findAll();
    @DeleteMapping(value = APP_ROOT +"/categories/delete/{idCategory}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Enregistrer une categorie", notes ="Cette méthode permet d'enregistrer ou modifier une categorie", response = CategoryDto.class)
    @ApiResponses(value ={
            @ApiResponse(code =200, message = "La categorie a ete cree / modifié"),
            @ApiResponse(code =400, message = "La categorie n'est pas valide")
    })
    void delete(@PathVariable("idCategory") Integer id);
}
