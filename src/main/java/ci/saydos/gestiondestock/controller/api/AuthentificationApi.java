package ci.saydos.gestiondestock.controller.api;

import ci.saydos.gestiondestock.dto.athentification.AuthentificationRequest;
import ci.saydos.gestiondestock.dto.athentification.AuthentificationResponse;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import static ci.saydos.gestiondestock.utils.Constants.AUTHENTIFICATION_ENDPOINT;

@Api(value = AUTHENTIFICATION_ENDPOINT,tags = {"authentication"})
@RequestMapping(AUTHENTIFICATION_ENDPOINT)
@CrossOrigin(origins = "http://localhost:4200")
public interface AuthentificationApi {
    @PostMapping("/authenticate")
    ResponseEntity<AuthentificationResponse> authenticate(@RequestBody AuthentificationRequest request);
}
