package ci.saydos.gestiondestock.controller.api;

import ci.saydos.gestiondestock.dto.ArticleDto;
import ci.saydos.gestiondestock.dto.LigneCommandeClientDto;
import ci.saydos.gestiondestock.dto.LigneCommandeFournisseurDto;
import ci.saydos.gestiondestock.dto.LigneVenteDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static ci.saydos.gestiondestock.utils.Constants.APP_ROOT;
@Api(value = APP_ROOT +"/articles",tags = {"articles"})
@CrossOrigin(origins = "http://localhost:4200")
public interface ArticleApi {

    @PostMapping(value = APP_ROOT +"/articles/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Enregistrer un article", notes ="Cette méthode permet d'enregistrer ou modifier un article", response = ArticleDto.class)
    @ApiResponses(value ={
            @ApiResponse(code =200, message = "L'objet article cree / modifié"),
            @ApiResponse(code =400, message = "L'article n'est pas valide")
    })
    ArticleDto save(@RequestBody ArticleDto articleDto);

    @GetMapping(value = APP_ROOT +"/articles/{idArticle}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Rechercher un article par son ID", notes ="Cette méthode permet de rechercher un article par son ID", response = ArticleDto.class)
    @ApiResponses(value ={
            @ApiResponse(code =200, message = "L'article a ete trouve dans la BDD"),
            @ApiResponse(code =400, message = "Aucun article n'existe dans la BDD avec l'ID fourni")
    })
    ArticleDto findById(@PathVariable("idArticle") Integer id);

    @GetMapping(value = APP_ROOT +"/articles/filter/{codeArticle}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Rechercher un article par son CODE", notes ="Cette méthode permet de rechercher un article par son CODE", response = ArticleDto.class)
    @ApiResponses(value ={
            @ApiResponse(code =200, message = "L'article a ete trouve dans la BDD"),
            @ApiResponse(code =400, message = "Aucun article n'existe dans la BDD avec le CODE fourni")})
    ArticleDto findByCode(@PathVariable("codeArticle") String code);

    @GetMapping(value = APP_ROOT +"/articles/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Renvoi la liste des articles", notes ="Cette méthode permet de rechercher et renvoyer la liste des articles qui existent",
            responseContainer = "List<ArticleDto>")
    @ApiResponses(value ={
            @ApiResponse(code =200, message = "L'article a ete trouve dans la BDD"),
            @ApiResponse(code =400, message = "Aucun article n'existe dans la BDD avec le CODE fourni")})
    List<ArticleDto> findAll();
    @GetMapping(value = APP_ROOT +"/articles/historique/vente/{idArticle}", produces = MediaType.APPLICATION_JSON_VALUE)
    List<LigneVenteDto> findHistoriqueVentes(@PathVariable("idArticle") Integer idArticle);
    @GetMapping(value = APP_ROOT +"/articles/historique/commandeClient/{idArticle}", produces = MediaType.APPLICATION_JSON_VALUE)
    List<LigneCommandeClientDto> findHistoriqueCommandeClient(Integer idArticle);
    @GetMapping(value = APP_ROOT +"/articles/historique/commandeFournisseur/{idArticle}", produces = MediaType.APPLICATION_JSON_VALUE)
    List<LigneCommandeFournisseurDto> findHistoriqueCommandeFournisseur(Integer idArticle);
    @GetMapping(value = APP_ROOT +"/articles/filter/category/filter/{idArticle}", produces = MediaType.APPLICATION_JSON_VALUE)
    List<ArticleDto> findAllArticleByIdCategory(Integer idCategory);
    @DeleteMapping(value = APP_ROOT + "/articles/delete/{idArticle}")
    @ApiOperation(value = "Supprimer un article", notes ="Cette méthode permet de supprimer un article par son ID",
            response = ArticleDto.class)
    @ApiResponses(value ={
            @ApiResponse(code =200, message = "L'article a ete supprimer")})
    void delete(@PathVariable("idArticle") Integer id);

}
