package ci.saydos.gestiondestock.controller.api;

import ci.saydos.gestiondestock.dto.CategoryDto;
import ci.saydos.gestiondestock.dto.FournisseurDto;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static ci.saydos.gestiondestock.utils.Constants.APP_ROOT;

@Api(value = APP_ROOT +"/fournisseurs",tags = {"fournisseurs"})
@CrossOrigin(origins = "http://localhost:4200")
public interface FournisseurApi {
    @PostMapping(APP_ROOT + "/fournisseurs/create")
    FournisseurDto save(@RequestBody FournisseurDto dto);
    @GetMapping(APP_ROOT + "/fournisseurs/{idFournisseur}")
    FournisseurDto findById(@PathVariable("idFournisseur") Integer id);
    @GetMapping(APP_ROOT + "/fournisseurs/all")
    List<FournisseurDto> findAll();
    @DeleteMapping(APP_ROOT + "/fournisseurs/delete/{idFournisseur}")
    void delete(@PathVariable("idFournisseur") Integer id);
}
