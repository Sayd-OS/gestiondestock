package ci.saydos.gestiondestock.controller.api;

import ci.saydos.gestiondestock.dto.RolesDto;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import static ci.saydos.gestiondestock.utils.Constants.APP_ROOT;

@Api(value = APP_ROOT +"/roles",tags = {"roles"})
@CrossOrigin(origins = "http://localhost:4200")
public interface RolesApi {
    @PostMapping(APP_ROOT +"/roles/create")
    RolesDto addNewRole(@RequestBody RolesDto role);

}
