package ci.saydos.gestiondestock.controller.api;

import ci.saydos.gestiondestock.dto.CommandeClientDto;
import ci.saydos.gestiondestock.dto.LigneCommandeClientDto;
import ci.saydos.gestiondestock.model.EtatCommande;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

import static ci.saydos.gestiondestock.utils.Constants.APP_ROOT;

@Api(value = APP_ROOT +"/commandeClients",tags = {"commandeClients"})
@CrossOrigin(origins = "http://localhost:4200")
public interface CommandeClientApi {
    @PostMapping(APP_ROOT + "/commandeClients/create")
    ResponseEntity<CommandeClientDto>  save(@RequestBody CommandeClientDto dto);
    @PatchMapping(APP_ROOT + "/commandeClients/update/etat/{idCommande}/{etatCommande}")
    ResponseEntity<CommandeClientDto> updateEtatCommande(@PathVariable("idCommande") Integer idCommande, @PathVariable("etatCommande") EtatCommande etatCommande);
    @PatchMapping(APP_ROOT + "/commandeClients/update/quantite/{idCommande}/{idLigneCommande}/{quantite}")
    ResponseEntity<CommandeClientDto> updateQuantiteCommande(@PathVariable("idCommande") Integer idCommande
            , @PathVariable("idLigneCommande") Integer idLigneCommande, @PathVariable("quantite") BigDecimal quantite);
    @PatchMapping(APP_ROOT + "/commandeClients/update/client/{idCommande}/{idClient}")
    ResponseEntity<CommandeClientDto> updateClient(@PathVariable("idCommande") Integer idCommande ,@PathVariable("idClient") Integer idClient);
    @PatchMapping(APP_ROOT + "/commandeClients/update/article/{idCommande}/{idLigneCommande}/{idArticle}")
    ResponseEntity<CommandeClientDto> updateArticle(@PathVariable("idCommande") Integer idCommande
            ,@PathVariable("idLigneCommande") Integer idLigneCommande,@PathVariable("idArticle") Integer idArticle);
    @DeleteMapping(APP_ROOT + "/commandeClients/delete/article/{idCommande}/{idLigneCommande}")
    ResponseEntity<CommandeClientDto> deleteArticle(@PathVariable("idCommande") Integer idCommande,@PathVariable("idLigneCommande") Integer idLigneCommande) ;
    @GetMapping(APP_ROOT + "/commandeClients/{idCommandeClient}")
    ResponseEntity<CommandeClientDto> findById(@PathVariable("idCommandeClient") Integer id);
    @GetMapping(APP_ROOT + "/commandeClients/{codeCommandeClient}")
    ResponseEntity<CommandeClientDto> findByCode(@PathVariable("codeCommandeClient") String code);
    @GetMapping(APP_ROOT + "/commandeClients/all")
    ResponseEntity<List<CommandeClientDto>> findAll();
    @GetMapping(APP_ROOT + "/commandeClients/ligneCommande/{idCommande}")
    ResponseEntity<List<LigneCommandeClientDto>> findAllLigneCommandesClientByCommandeClientId(@PathVariable("idCommande") Integer idCommande);
    @DeleteMapping(APP_ROOT + "/commandeClients/delete/client/{idCommandeClient}")
    ResponseEntity<Void> delete(@PathVariable("idCommandeClient") Integer id);
}
