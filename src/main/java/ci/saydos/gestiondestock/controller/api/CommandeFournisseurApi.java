package ci.saydos.gestiondestock.controller.api;

import ci.saydos.gestiondestock.dto.CommandeFournisseurDto;
import ci.saydos.gestiondestock.dto.CommandeFournisseurDto;
import ci.saydos.gestiondestock.dto.LigneCommandeFournisseurDto;
import ci.saydos.gestiondestock.dto.LigneCommandeFournisseurDto;
import ci.saydos.gestiondestock.model.EtatCommande;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

import static ci.saydos.gestiondestock.utils.Constants.APP_ROOT;

@Api(value = APP_ROOT +"/commandeFournisseurs",tags = {"commandeFournisseur"})
@CrossOrigin(origins = "http://localhost:4200")
public interface CommandeFournisseurApi {
    @PostMapping(APP_ROOT + "/commandeFournisseurs/create")
    ResponseEntity<CommandeFournisseurDto> save(@RequestBody CommandeFournisseurDto dto);
    @PatchMapping(APP_ROOT + "/commandeFournisseurs/update/etat/{idCommande}/{etatCommande}")
    ResponseEntity<CommandeFournisseurDto> updateEtatCommande(@PathVariable("idCommande") Integer idCommande, @PathVariable("etatCommande") EtatCommande etatCommande);
    @PatchMapping(APP_ROOT + "/commandeFournisseurs/update/quantite/{idCommande}/{idLigneCommande}/{quantite}")
    ResponseEntity<CommandeFournisseurDto> updateQuantiteCommande(@PathVariable("idCommande") Integer idCommande
            , @PathVariable("idLigneCommande") Integer idLigneCommande, @PathVariable("quantite") BigDecimal quantite);
    @PatchMapping(APP_ROOT + "/commandeFournisseurs/update/fournisseur/{idCommande}/{idFournisseur}")
    ResponseEntity<CommandeFournisseurDto> updateFournisseur(@PathVariable("idCommande") Integer idCommande ,@PathVariable("idFournisseur") Integer idFournisseur);
    @PatchMapping(APP_ROOT + "/commandeFournisseurs/update/article/{idCommande}/{idLigneCommande}/{idArticle}")
    ResponseEntity<CommandeFournisseurDto> updateArticle(@PathVariable("idCommande") Integer idCommande
            ,@PathVariable("idLigneCommande") Integer idLigneCommande,@PathVariable("idArticle") Integer idArticle);
    @DeleteMapping(APP_ROOT + "/commandeFournisseurs/delete/article/{idCommande}/{idLigneCommande}")
    ResponseEntity<CommandeFournisseurDto> deleteArticle(@PathVariable("idCommande") Integer idCommande,@PathVariable("idLigneCommande") Integer idLigneCommande) ;
    @GetMapping(APP_ROOT + "/commandeFournisseurs/{idCommandeFournisseur}")
    ResponseEntity<CommandeFournisseurDto> findById(@PathVariable("idCommandeFournisseur") Integer id);
    @GetMapping(APP_ROOT + "/commandeFournisseurs/{codeCommandeFournisseur}")
    ResponseEntity<CommandeFournisseurDto> findByCode(@PathVariable("codeCommandeFournisseur") String code);
    @GetMapping(APP_ROOT + "/commandeFournisseurs/all")
    ResponseEntity<List<CommandeFournisseurDto>> findAll();
    @GetMapping(APP_ROOT + "/commandeFournisseurs/ligneCommande/{idCommande}")
    ResponseEntity<List<LigneCommandeFournisseurDto>> findAllLigneCommandesFournisseurByCommandeFournisseurId(@PathVariable("idCommande") Integer idCommande);
    @DeleteMapping(APP_ROOT + "/commandeFournisseurs/delete/fournisseur/{idCommandeFournisseur}")
    ResponseEntity<Void> delete(@PathVariable("idCommandeFournisseur") Integer id);

}
