package ci.saydos.gestiondestock.controller.api;

import ci.saydos.gestiondestock.dto.ChangerMotDePasseUtilisateurDto;
import ci.saydos.gestiondestock.dto.UtilisateurDto;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static ci.saydos.gestiondestock.utils.Constants.APP_ROOT;

@Api(value = APP_ROOT +"/utilisateurs",tags = {"utilisateurs"})
@CrossOrigin(origins = "http://localhost:4200")
public interface UtilisateurApi {
    @PostMapping(APP_ROOT + "/utilisateurs/create")
    UtilisateurDto save(@RequestBody UtilisateurDto dto);
    @GetMapping(APP_ROOT + "/utilisateurs/{idUtilisateur}")
    UtilisateurDto findById(@PathVariable("idUtilisateur") Integer id);
    @GetMapping(APP_ROOT + "/utilisateurs/all")
    List<UtilisateurDto> findAll();
    @GetMapping(APP_ROOT + "/utilisateurs/{email}")
    UtilisateurDto findByEmail(@PathVariable("email") String email);
    @PostMapping(APP_ROOT + "/utilisateurs/changeMotDePasse")
    UtilisateurDto changeMotDePasse(@RequestBody  ChangerMotDePasseUtilisateurDto dto);
    @DeleteMapping(APP_ROOT + "/utilisateurs/{idUtilisateur}")
    void delete(@PathVariable("idUtilisateur") Integer id);
}
