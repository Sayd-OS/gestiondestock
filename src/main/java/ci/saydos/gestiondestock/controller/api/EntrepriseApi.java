package ci.saydos.gestiondestock.controller.api;

import ci.saydos.gestiondestock.dto.EntrepriseDto;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static ci.saydos.gestiondestock.utils.Constants.APP_ROOT;

@Api(value = APP_ROOT +"/entreprises", tags = {"entreprises"})
@CrossOrigin(origins = "http://localhost:4200")
public interface EntrepriseApi {
    @PostMapping(APP_ROOT + "/entreprises/create")
    ResponseEntity<EntrepriseDto> save(@RequestBody EntrepriseDto dto);
    @GetMapping(APP_ROOT + "/entreprises/{idEntreprise}")
    ResponseEntity<EntrepriseDto> findById(@PathVariable("idEntreprise") Integer id);
    @GetMapping(APP_ROOT + "/entreprises/all")
    ResponseEntity<List<EntrepriseDto>> findAll();
    @DeleteMapping(APP_ROOT + "/entreprises/delete/{idEntreprise}")
    ResponseEntity<Void> delete(@PathVariable("idEntreprise") Integer id);
}
