package ci.saydos.gestiondestock.controller;


import ci.saydos.gestiondestock.controller.api.AuthentificationApi;
import ci.saydos.gestiondestock.dto.athentification.AuthentificationRequest;
import ci.saydos.gestiondestock.dto.athentification.AuthentificationResponse;
import ci.saydos.gestiondestock.model.authentification.ExtendedUser;
import ci.saydos.gestiondestock.services.auth.ApplicationUserDetailsService;
import ci.saydos.gestiondestock.utils.Jwt;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static ci.saydos.gestiondestock.utils.Constants.AUTHENTIFICATION_ENDPOINT;

@RestController
@AllArgsConstructor
public class AuthentificationController implements AuthentificationApi {

    @Autowired
    private ApplicationUserDetailsService userDetailsService;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private Jwt jwt;

    public ResponseEntity<AuthentificationResponse> authenticate(@RequestBody AuthentificationRequest request) {

        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(request.getLogin(), request.getPassword());

        Authentication autM = authenticationManager.authenticate(authenticationToken);

        UserDetails user =  userDetailsService.loadUserByUsername(request.getLogin());
        final String jwtAccessToken = jwt.generateAccessToken((ExtendedUser) user);
        return ResponseEntity.ok(AuthentificationResponse.builder().accessToken(jwtAccessToken).build());

    }


}
