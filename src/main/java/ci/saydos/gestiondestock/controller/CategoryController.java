package ci.saydos.gestiondestock.controller;

import ci.saydos.gestiondestock.controller.api.CategoryApi;
import ci.saydos.gestiondestock.dto.CategoryDto;
import ci.saydos.gestiondestock.services.implementation.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
public class CategoryController implements CategoryApi {
    @Autowired
    private CategoryService categoryService;

    @Override
    public CategoryDto save(CategoryDto dto) {
        return categoryService.save(dto);
    }

    @Override
    public CategoryDto findById(Integer id) {
        return categoryService.findById(id);
    }

    @Override
    public CategoryDto findByCode(String code) {
        return categoryService.findByCode(code);
    }

    @Override
    public List<CategoryDto> findAll() {
        return categoryService.findAll();
    }

    @Override
    public void delete(Integer id) {
     categoryService.delete(id);
    }
}
