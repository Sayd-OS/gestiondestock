package ci.saydos.gestiondestock.controller;

import ci.saydos.gestiondestock.controller.api.RolesApi;
import ci.saydos.gestiondestock.dto.RolesDto;
import ci.saydos.gestiondestock.repository.RolesRepository;
import ci.saydos.gestiondestock.services.implementation.RolesService;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RolesController implements RolesApi {
    private  RolesService rolesService;
    private final RolesRepository rolesRepository;

    public RolesController(RolesService rolesService,
                           RolesRepository rolesRepository) {
        this.rolesService = rolesService;
        this.rolesRepository = rolesRepository;
    }

    @Override
    public RolesDto addNewRole(RolesDto role) {
        return rolesService.addNewRole(role);
    }


}
