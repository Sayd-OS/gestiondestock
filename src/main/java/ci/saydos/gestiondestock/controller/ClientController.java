package ci.saydos.gestiondestock.controller;

import ci.saydos.gestiondestock.controller.api.ClientApi;
import ci.saydos.gestiondestock.dto.CategoryDto;
import ci.saydos.gestiondestock.dto.ClientDto;
import ci.saydos.gestiondestock.services.implementation.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ClientController implements ClientApi {

    @Autowired
    private ClientService clientService;

    @Override
    public ClientDto save(ClientDto dto) {
        return clientService.save(dto);
    }

    @Override
    public ClientDto findById(Integer id) {
        return clientService.findById(id);
    }

    @Override
    public List<ClientDto> findAll() {
        return clientService.findAll();
    }

    @Override
    public void delete(Integer id) {
        clientService.delete(id);
    }
}
