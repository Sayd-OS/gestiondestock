package ci.saydos.gestiondestock.dto;

import ci.saydos.gestiondestock.model.AbstractEntity;
import ci.saydos.gestiondestock.model.Category;
import ci.saydos.gestiondestock.model.LigneCommandeClient;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Builder
public class LigneCommandeClientDto {

    private Integer id;
    private Integer idEntreprise;
    private ArticleDto article;
    @JsonIgnore
    private CommandeClientDto commandeClient;
    private BigDecimal quantite;
    private BigDecimal prixUnitaire;

    public static LigneCommandeClientDto fromEntity(LigneCommandeClient entity){
        if (entity == null) {
            return null;
        }

        return LigneCommandeClientDto.builder()
                .id(entity.getId())
                .idEntreprise(entity.getIdEntreprise())
                .article(ArticleDto.fromEntity(entity.getArticle()))
                .quantite(entity.getQuantite())
                .prixUnitaire(entity.getPrixUnitaire())
                .build();

    }

    public static LigneCommandeClient toEntity(LigneCommandeClientDto dto){
        if (dto == null) {
            return null;
        }

        LigneCommandeClient ligneCommandeClient = new LigneCommandeClient();
        ligneCommandeClient.setId(dto.getId());
        ligneCommandeClient.setIdEntreprise(dto.getIdEntreprise());
        ligneCommandeClient.setArticle(ArticleDto.toEntity(dto.getArticle()));
        ligneCommandeClient.setQuantite(dto.getQuantite());
        ligneCommandeClient.setPrixUnitaire(dto.getPrixUnitaire());

        return ligneCommandeClient;
    }


}
