package ci.saydos.gestiondestock.dto;

import ci.saydos.gestiondestock.model.Category;
import ci.saydos.gestiondestock.model.Client;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ClientDto {
    private Integer id;
    private Integer idEntreprise;
    private String nom;
    private String prenom;
    private String mail;
    private AdresseDto adresse;
    private String photo;
    private String numTel;
    @JsonIgnore
    private List<CommandeClientDto> commandeClients;

    public static ClientDto fromEntity(Client client){
        if (client == null) {
            return null;
        }

        return ClientDto.builder()
                .id(client.getId())
                .idEntreprise(client.getIdEntreprise())
                .nom(client.getNom())
                .prenom(client.getPrenom())
                .mail(client.getMail())
                .adresse(AdresseDto.fromEntity(client.getAdresse()))
                .photo(client.getPhoto())
                .numTel(client.getNumTel())
                .build();
    }

    public static Client toEntity(ClientDto clientDto){
        if (clientDto == null) {
            return null;
        }

        Client client = new Client();
        client.setId(clientDto.getId());
        client.setIdEntreprise(clientDto.getIdEntreprise());
        client.setNom(clientDto.getNom());
        client.setPrenom(clientDto.getPrenom());
        client.setMail(clientDto.getMail());
        client.setAdresse(AdresseDto.toEntity(clientDto.getAdresse()));
        client.setPhoto(clientDto.getPhoto());
        client.setNumTel(clientDto.getNumTel());

        return client;
    }

}
