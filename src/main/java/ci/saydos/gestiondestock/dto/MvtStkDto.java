package ci.saydos.gestiondestock.dto;

import ci.saydos.gestiondestock.model.MvtStk;
import ci.saydos.gestiondestock.model.SourceMvtStk;
import ci.saydos.gestiondestock.model.TypeMvtStk;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.Instant;

@Data
@Builder
public class MvtStkDto {
    private Integer id;
    private Integer idEntreprise;
    private ArticleDto article;
    private Instant dateMvt;
    private TypeMvtStk typeMvtStk;
    private SourceMvtStk sourceMvtStk;
    private BigDecimal quantite;
    private BigDecimal prixUnitaire;

    public static MvtStkDto fromEntity(MvtStk entity) {
        if (entity == null) {
            return null;
        }

        return MvtStkDto.builder()
                .id(entity.getId())
                .idEntreprise(entity.getIdEntreprise())
                .article(ArticleDto.fromEntity(entity.getArticle()))
                .dateMvt(entity.getDateMvt())
                .typeMvtStk(entity.getTypeMvtStk())
                .sourceMvtStk(entity.getSourceMvtStk())
                .quantite(entity.getQuantite())
                .prixUnitaire(entity.getPrixUnitaire())
                .build();
    }

    public static MvtStk toEntity(MvtStkDto dto) {
        if (dto == null) {
            return null;
        }
        MvtStk mvStk = new MvtStk();
        mvStk.setId(dto.getId());
        mvStk.setIdEntreprise(dto.getIdEntreprise());
        mvStk.setArticle(dto.getArticle().toEntity(dto.getArticle()));
        mvStk.setDateMvt(dto.getDateMvt());
        mvStk.setTypeMvtStk(dto.getTypeMvtStk());
        mvStk.setSourceMvtStk(dto.getSourceMvtStk());
        mvStk.setQuantite(dto.getQuantite());
        mvStk.setPrixUnitaire(dto.getPrixUnitaire());
        return mvStk;
    }

}

