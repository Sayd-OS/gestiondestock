package ci.saydos.gestiondestock.dto;

import ci.saydos.gestiondestock.model.AbstractEntity;
import ci.saydos.gestiondestock.model.Category;
import ci.saydos.gestiondestock.model.Client;
import ci.saydos.gestiondestock.model.Fournisseur;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
public class FournisseurDto {
    private Integer id;
    private Integer idEntreprise;
    private String nom;
    private String prenom;
    private String mail;
    private AdresseDto adresse;
    private String photo;
    private String numTel;
    private List<CommandeFournisseurDto> commandeFournisseurs;

    public static FournisseurDto fromEntity(Fournisseur entity){
        if (entity == null) {
            return null;
        }

        return FournisseurDto.builder()
                .id(entity.getId())
                .idEntreprise(entity.getIdEntreprise())
                .nom(entity.getNom())
                .prenom(entity.getPrenom())
                .mail(entity.getMail())
                .adresse(AdresseDto.fromEntity(entity.getAdresse()))
                .photo(entity.getPhoto())
                .numTel(entity.getNumTel())
                .build();
    }

    public static Fournisseur toEntity(FournisseurDto dto){
        if (dto == null) {
            return null;
        }

        Fournisseur fournisseur = new Fournisseur();
        fournisseur.setId(dto.getId());
        fournisseur.setIdEntreprise(dto.getIdEntreprise());
        fournisseur.setNom(dto.getNom());
        fournisseur.setPrenom(dto.getPrenom());
        fournisseur.setMail(dto.getMail());
        fournisseur.setAdresse(AdresseDto.toEntity(dto.getAdresse()));
        fournisseur.setPhoto(dto.getPhoto());
        fournisseur.setNumTel(dto.getNumTel());

        return fournisseur;
    }
}
