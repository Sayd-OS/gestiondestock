package ci.saydos.gestiondestock.dto;

import ci.saydos.gestiondestock.model.Adresse;
import lombok.*;

import javax.persistence.Embeddable;

@Data
@Builder
public class AdresseDto {
    private String adresse1;
    private String adresse2;
    private String ville;
    private String codePostale;
    private String pays;

    public static AdresseDto fromEntity(Adresse adresse){
        if (adresse == null) {
            return null;
        }
        return AdresseDto.builder()
                .adresse1(adresse.getAdresse1())
                .adresse2(adresse.getAdresse2())
                .ville(adresse.getVille())
                .codePostale(adresse.getCodePostale())
                .pays(adresse.getPays())
                .build();
    }

    public static Adresse toEntity(AdresseDto categoryDto){
        if (categoryDto == null) {
            return null;
        }

        Adresse adresse = new Adresse();
        adresse.setAdresse1(categoryDto.getAdresse1());
        adresse.setAdresse2(categoryDto.getAdresse2());
        adresse.setCodePostale(categoryDto.getCodePostale());
        adresse.setVille(categoryDto.getVille());
        adresse.setPays(categoryDto.pays);
        return adresse;
    }
}
