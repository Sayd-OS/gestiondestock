package ci.saydos.gestiondestock.dto;

import ci.saydos.gestiondestock.model.AbstractEntity;
import ci.saydos.gestiondestock.model.Category;
import ci.saydos.gestiondestock.model.CommandeClient;
import ci.saydos.gestiondestock.model.EtatCommande;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Data
@Builder
public class CommandeClientDto  {
    private Integer id;
    private Integer idEntreprise;
    private String code;
    private Instant dateCommande;
    private EtatCommande etatCommande;
    private ClientDto client;

    @JsonIgnore
    private List<LigneCommandeClientDto> ligneCommandeClients;

    public static CommandeClientDto fromEntity(CommandeClient entity){
        if (entity == null) {
            return null;
        }

        return CommandeClientDto.builder()
                .id(entity.getId())
                .idEntreprise(entity.getIdEntreprise())
                .code(entity.getCode())
                .dateCommande(entity.getDateCommande())
                .etatCommande(entity.getEtatCommande())
                .client(ClientDto.fromEntity(entity.getClient()))
                .build();
    }

    public static CommandeClient toEntity(CommandeClientDto dto){
        if (dto == null) {
            return null;
        }

        CommandeClient commandeClient = new CommandeClient();
        commandeClient.setId(dto.getId());
        commandeClient.setIdEntreprise(dto.getIdEntreprise());
        commandeClient.setClient(ClientDto.toEntity(dto.getClient()));
        commandeClient.setEtatCommande(dto.getEtatCommande());
        commandeClient.setDateCommande(dto.getDateCommande());
        commandeClient.setCode(dto.getCode());
        return commandeClient;
    }

    public  boolean isCommandeLivree() {
        return etatCommande.LIVREE.equals(this.etatCommande);
    }


}
