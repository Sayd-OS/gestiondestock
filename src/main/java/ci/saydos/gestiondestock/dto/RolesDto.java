package ci.saydos.gestiondestock.dto;

import ci.saydos.gestiondestock.model.Category;
import ci.saydos.gestiondestock.model.Roles;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RolesDto {
    private Integer id;
    private String roleName;
    @JsonIgnore
    private UtilisateurDto utilisateur;

    public static RolesDto fromEntity(Roles entity) {
        if (entity == null) {
            return null;
        }

        return RolesDto.builder()
                .id(entity.getId())
                .roleName(entity.getRoleName())
                .build();
    }

    public static Roles toEntity(RolesDto dto) {
        if (dto == null) {
            return null;
        }

        Roles roles = new Roles();
        roles.setId(dto.getId());
        roles.setRoleName(dto.getRoleName());
        return roles;
    }


}

