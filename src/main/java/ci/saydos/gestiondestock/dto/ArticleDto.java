package ci.saydos.gestiondestock.dto;

import ci.saydos.gestiondestock.model.Article;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
public class ArticleDto {
    private Integer id;
    private Integer idEntreprise;
    private String codeArticle;
    private String designation;
    private BigDecimal prixUnitaireHt;
    private BigDecimal prixUnitaireTtc;
    private BigDecimal tauxTva;
    private String photo;
    private CategoryDto category;
    @JsonIgnore
    private List<ArticleDto> articles;

    public static ArticleDto fromEntity(Article article){
        if (article == null) {
            return null;
        }

        return ArticleDto.builder()
                .id(article.getId())
                .idEntreprise(article.getIdEntreprise())
                .codeArticle(article.getCodeArticle())
                .designation(article.getDesignation())
                .prixUnitaireHt(article.getPrixUnitaireHt())
                .prixUnitaireTtc(article.getPrixUnitaireTtc())
                .tauxTva(article.getTauxTva())
                .photo(article.getPhoto())
                .category(CategoryDto.fromEntity(article.getCategory()))
                .build();
    }

    public static Article toEntity(ArticleDto articleDto){
        if (articleDto == null) {
            return null;
        }

        Article article = new Article();
        article.setId(articleDto.getId());
        article.setIdEntreprise(articleDto.getIdEntreprise());
        article.setCodeArticle(articleDto.getCodeArticle());
        article.setDesignation(articleDto.getDesignation());
        article.setPrixUnitaireHt(articleDto.getPrixUnitaireHt());
        article.setPrixUnitaireTtc(articleDto.getPrixUnitaireTtc());
        article.setTauxTva(articleDto.getTauxTva());
        article.setPhoto(articleDto.getPhoto());
        article.setCategory(CategoryDto.toEntity(articleDto.getCategory()));

        return article;
    }
}
