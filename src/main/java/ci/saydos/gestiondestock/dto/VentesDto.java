package ci.saydos.gestiondestock.dto;

import ci.saydos.gestiondestock.model.Category;
import ci.saydos.gestiondestock.model.CommandeClient;
import ci.saydos.gestiondestock.model.Ventes;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
@Builder
public class VentesDto {
    private Integer id;
    private Integer idEntreprise;
    private String code;
    private Instant dateVente;
    private String commentaire;
    @JsonIgnore
    private List<LigneVenteDto> ligneVente;

    public static VentesDto fromEntity(Ventes entity){
        if (entity == null) {
            return null;
        }

        return VentesDto.builder()
                .id(entity.getId())
                .idEntreprise(entity.getIdEntreprise())
                .code(entity.getCode())
                .dateVente(entity.getDateVente())
                .commentaire(entity.getCommentaire())
                .build();
    }

    public static Ventes toEntity(VentesDto dto){
        if (dto == null) {
            return null;
        }

        Ventes ventes = new Ventes();
        ventes.setId(dto.getId());
        ventes.setIdEntreprise(dto.getIdEntreprise());
        ventes.setCode(dto.getCode());
        ventes.setDateVente(dto.getDateVente());
        ventes.setCommentaire(dto.getCommentaire());

        return ventes;
    }
}
