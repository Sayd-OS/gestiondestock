package ci.saydos.gestiondestock.dto;

import ci.saydos.gestiondestock.model.AbstractEntity;
import ci.saydos.gestiondestock.model.Category;
import ci.saydos.gestiondestock.model.LigneCommandeClient;
import ci.saydos.gestiondestock.model.LigneCommandeFournisseur;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Builder
public class LigneCommandeFournisseurDto {
    private Integer id;
    private Integer idEntreprise;
    private ArticleDto article;
    @JsonIgnore
    private CommandeClientDto commandeFournisseur;
    private BigDecimal quantite;
    private BigDecimal prixUnitaire;

    public static LigneCommandeFournisseurDto fromEntity(LigneCommandeFournisseur entity){
        if (entity == null) {
            return null;
        }

        return LigneCommandeFournisseurDto.builder()
                .id(entity.getId())
                .idEntreprise(entity.getIdEntreprise())
                .article(ArticleDto.fromEntity(entity.getArticle()))
                .quantite(entity.getQuantite())
                .prixUnitaire(entity.getPrixUnitaire())
                .build();

    }

    public static LigneCommandeFournisseur toEntity(LigneCommandeFournisseurDto dto){
        if (dto == null) {
            return null;
        }

        LigneCommandeFournisseur ligneCommandeFournisseur = new LigneCommandeFournisseur();
        ligneCommandeFournisseur.setId(dto.getId());
        ligneCommandeFournisseur.setIdEntreprise(dto.getIdEntreprise());
        ligneCommandeFournisseur.setArticle(ArticleDto.toEntity(dto.getArticle()));
        ligneCommandeFournisseur.setQuantite(dto.getQuantite());
        ligneCommandeFournisseur.setPrixUnitaire(dto.getPrixUnitaire());

        return ligneCommandeFournisseur;
    }
}
