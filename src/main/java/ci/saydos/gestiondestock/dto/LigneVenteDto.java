package ci.saydos.gestiondestock.dto;

import ci.saydos.gestiondestock.model.AbstractEntity;
import ci.saydos.gestiondestock.model.Category;
import ci.saydos.gestiondestock.model.LigneVente;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
public class LigneVenteDto {
    private Integer id;
    private Integer idEntreprise;
    private VentesDto vente;
    private ArticleDto article;
    private BigDecimal quantite;
    private BigDecimal prixUnitaire;


    public static LigneVenteDto fromEntity(LigneVente entity) {
        if (entity == null) {
            return null;
        }

        return LigneVenteDto.builder()
                .id(entity.getId())
                .idEntreprise(entity.getIdEntreprise())
                .vente(VentesDto.fromEntity(entity.getVente()))
                .article(ArticleDto.fromEntity(entity.getArticle()))
                .quantite(entity.getQuantite())
                .prixUnitaire(entity.getPrixUnitaire())
                .build();
    }

    public static LigneVente toEntity(LigneVenteDto dto) {
        if (dto == null) {
            return null;
        }

        LigneVente ligneVente = new LigneVente();
        ligneVente.setId(dto.getId());
        ligneVente.setIdEntreprise(dto.getIdEntreprise());
        ligneVente.setVente(VentesDto.toEntity(dto.getVente()));
        ligneVente.setArticle(ArticleDto.toEntity(dto.getArticle()));
        ligneVente.setQuantite(dto.getQuantite());
        ligneVente.setPrixUnitaire(dto.getPrixUnitaire());
        return ligneVente;
    }
}

