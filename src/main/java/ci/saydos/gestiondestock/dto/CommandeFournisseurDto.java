package ci.saydos.gestiondestock.dto;

import ci.saydos.gestiondestock.model.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Data
@Builder
public class CommandeFournisseurDto {

    private Integer id;
    private Integer idEntreprise;
    private String code;
    private Instant dateCommande;
    private EtatCommande etatCommande;
    private FournisseurDto fournisseur;
    @JsonIgnore
    private List<LigneCommandeFournisseurDto> ligneCommandeFournisseurs;

    public static CommandeFournisseurDto fromEntity(CommandeFournisseur entity){
        if (entity == null) {
            return null;
        }

        return CommandeFournisseurDto.builder()
                .id(entity.getId())
                .idEntreprise(entity.getIdEntreprise())
                .code(entity.getCode())
                .dateCommande(entity.getDateCommande())
                .etatCommande(entity.getEtatCommande())
                .fournisseur(FournisseurDto.fromEntity(entity.getFournisseur()))
                .build();
    }

    public static CommandeFournisseur toEntity(CommandeFournisseurDto dto){
        if (dto == null) {
            return null;
        }

        CommandeFournisseur commandeFournisseur = new CommandeFournisseur();
        commandeFournisseur.setId(dto.getId());
        commandeFournisseur.setIdEntreprise(dto.getIdEntreprise());
        commandeFournisseur.setFournisseur(FournisseurDto.toEntity(dto.getFournisseur()));
        commandeFournisseur.setEtatCommande(dto.getEtatCommande());
        commandeFournisseur.setDateCommande(dto.getDateCommande());
        commandeFournisseur.setCode(dto.getCode());
        return commandeFournisseur;
    }

    public boolean isCommandeLivree() {
        return etatCommande.LIVREE.equals(this.etatCommande);
    }
}
