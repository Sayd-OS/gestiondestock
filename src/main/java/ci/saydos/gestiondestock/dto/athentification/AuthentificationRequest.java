package ci.saydos.gestiondestock.dto.athentification;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AuthentificationRequest {
    private String login;
    private String password;
}
