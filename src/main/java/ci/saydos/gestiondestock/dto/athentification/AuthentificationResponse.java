package ci.saydos.gestiondestock.dto.athentification;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AuthentificationResponse {
    private String accessToken;
}
