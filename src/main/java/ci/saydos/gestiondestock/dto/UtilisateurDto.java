package ci.saydos.gestiondestock.dto;

import ci.saydos.gestiondestock.model.Category;
import ci.saydos.gestiondestock.model.Utilisateur;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class UtilisateurDto {
    private Integer id;
    private String nom;
    private String prenom;
    private String email;
    private Instant dateDeNaissance;
    private String motDePasse;
    private AdresseDto adresse;
    private String photo;
    private EntrepriseDto entreprise;
    private List<RolesDto> roles;

    public static UtilisateurDto fromEntity(Utilisateur entity) {
        if (entity == null) {
            return null;
        }

        return UtilisateurDto.builder()
                .id(entity.getId())
                .nom(entity.getNom())
                .prenom(entity.getPrenom())
                .email(entity.getEmail())
                .dateDeNaissance(entity.getDateDeNaissance())
                .motDePasse(entity.getMotDePasse())
                .adresse(AdresseDto.fromEntity(entity.getAdresse()))
                .entreprise(EntrepriseDto.fromEntity(entity.getEntreprise()))
                .roles(entity.getRoles().stream()
                                .map(RolesDto::fromEntity)
                                .collect(Collectors.toList())
                )
                .photo(entity.getPhoto())
                .build();

    }

    public static Utilisateur toEntity(UtilisateurDto dto) {
        if (dto == null) {
            return null;
        }

        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setId(dto.getId());
        utilisateur.setNom(dto.getNom());
        utilisateur.setPrenom(dto.getPrenom());
        utilisateur.setEmail(dto.getEmail());
        utilisateur.setDateDeNaissance(dto.getDateDeNaissance());
        utilisateur.setMotDePasse(dto.getMotDePasse());
        utilisateur.setAdresse(AdresseDto.toEntity(dto.getAdresse()));
        utilisateur.setEntreprise(EntrepriseDto.toEntity(dto.getEntreprise()));
        utilisateur.setPhoto(dto.getPhoto());
        utilisateur.setRoles(dto.getRoles() != null ? dto.getRoles().stream()
                .map(RolesDto::toEntity)
                .collect(Collectors.toList()) : null);
        return utilisateur;
    }

}
