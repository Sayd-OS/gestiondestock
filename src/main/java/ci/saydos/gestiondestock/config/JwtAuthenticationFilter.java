//package ci.saydos.gestiondestock.config;
//
//import ci.saydos.gestiondestock.model.authentification.ExtendedUser;
//import com.auth0.jwt.JWT;
//import com.auth0.jwt.algorithms.Algorithm;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.stream.Collectors;
//
//import static ci.saydos.gestiondestock.utils.Constants.*;
//
//public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
//    private  AuthenticationManager authenticationManager;
//
//    public JwtAuthenticationFilter(AuthenticationManager authenticationManager) {
//        this.authenticationManager = authenticationManager;
//    }
//
//    @Override
//    //Lorsque essaie de s'authentifie
//    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
//        // Lorsqu'on on saisie dans une formulaire
//        System.out.println("attemptAuthentication");
//        String username = null ;
//        String password = null ;
//        try {
//            Map<String,String> requestMap = new ObjectMapper().readValue(request.getInputStream(), Map.class);
//                   username = requestMap.get("login");
//                   password = requestMap.get("password");
//            System.out.println(username + " " + password);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//
//        UsernamePasswordAuthenticationToken authenticationToken =
//                new UsernamePasswordAuthenticationToken(username, password);
//
//        return authenticationManager.authenticate(authenticationToken);
//    }
//
//    @Override
//    //Lorsque l'authentification reussie
//    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
//        System.out.println("SuccessfulAuthentication");
//        ExtendedUser user = (ExtendedUser) authResult.getPrincipal();
//        Algorithm algo1 = Algorithm.HMAC256(SECRET);
//        String jwtAccessToken = JWT.create()
//                .withSubject(user.getUsername()) //
//                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRE_ACCESS_TOKEN))
//                .withClaim("roles", user.getAuthorities().stream().map(ga -> ga.getAuthority()).collect(Collectors.toList()))
//                .withClaim("idEntreprise",user.getIdEntreprise().toString())
//                .sign(algo1);
//
//        Map<String,String> idToken = new HashMap<>();
//        idToken.put("access-Token", jwtAccessToken);
//        response.setContentType("application/json");
//        new ObjectMapper().writeValue(response.getOutputStream(),idToken);
//    }
//}
